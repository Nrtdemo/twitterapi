﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using TwitterApi.Models;

namespace TwitterApi.Migrations
{
    [DbContext(typeof(TwitterContext))]
    partial class TwitterContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1");

            modelBuilder.Entity("TwitterApi.Models.Following", b =>
                {
                    b.Property<int>("FollowingID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Follow");

                    b.Property<int>("User");

                    b.HasKey("FollowingID");

                    b.ToTable("Followings");
                });

            modelBuilder.Entity("TwitterApi.Models.Tweet", b =>
                {
                    b.Property<int>("TweetID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Message");

                    b.Property<int>("Owner");

                    b.Property<DateTime>("WhenCreated");

                    b.HasKey("TweetID");

                    b.ToTable("Tweets");
                });

            modelBuilder.Entity("TwitterApi.Models.User", b =>
                {
                    b.Property<int>("UserID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("UserID");

                    b.ToTable("Users");
                });
        }
    }
}
